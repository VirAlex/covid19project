import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import StatsPays from "@/components/StatsPays"
import GMap from "@/components/map/GMap"

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      name: "Home",
      component: Home
    },
    {
      path: "/stats-pays",
      name: "StatsPays",
      component: StatsPays
    },
    {
      path: "/map",
      name: "Gmap",
      component: GMap
    }
  ]
});
